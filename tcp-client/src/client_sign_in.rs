/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/





extern crate rpassword;

use integrity;


pub fn sign_in() -> String {


    let mut full_request = "sign_in_state::Sign-in**".to_owned();

    println!("===============================\n\tSIGN IN\n\n" );
    let username = rpassword::prompt_response_stdout("username: ").unwrap();


    full_request.push_str(&username);
    full_request.push_str("--");

    let password = rpassword::prompt_password_stdout("password: ").unwrap();

    let hashed_password = integrity::sha3_512(password);

    full_request.push_str(&hashed_password);


    full_request.to_string()

}

pub fn decisive_test() -> String {

    let message = rpassword::prompt_response_stdout("Enclosed Message: ").unwrap();

    message
}
