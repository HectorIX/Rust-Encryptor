/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/



use user;
use parser;
use file_io;
use interface;


pub fn download() -> String {

    let mut full_request = "download_state::Download**".to_owned();

    let username = user::get_username();
    let filename = interface::read_filename();

    full_request.push_str(&username);
    full_request.push_str("--");
    full_request.push_str(&user::get_session_key());
    full_request.push_str("#!?#");
    full_request.push_str(&filename);

    full_request
}




pub fn store_file_locally( data: String ) -> bool {


    let mut path_to_file = "download/".to_string();


    if data.len() > 0 {

        let (session_key, file_data) = parser::split_session_key(data);
        let (filename, file_context) = parser::extract_file_context(file_data);

        if session_key == user::get_session_key() {

            path_to_file.push_str(&filename);

            file_io::write_file(path_to_file, file_context);

            return true;
        }
        else {

            return false;
        }
    }
    else {

        return false;
    }

}
