/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/




use std::str;

use user;
use file_io;
use interface;


pub fn upload() -> String {

    let mut full_request = "upload_state::Upload**".to_owned();
    let mut path_to_file = "to_upload/".to_string();

    let filename = interface::read_filename();

    path_to_file.push_str(&filename);

    let file_context = file_io::read_file(path_to_file.to_string());
    //let x = file_io::read_u8(path_to_file.clone().to_string());

    let username = user::get_username();

    full_request.push_str(&username);
    full_request.push_str("--");
    full_request.push_str(&user::get_session_key());
    full_request.push_str("#!?#");
    full_request.push_str(&filename);
    full_request.push_str("<$$>");
    full_request.push_str(&file_context);


    full_request
}
