/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/




extern crate rpassword;


use std::io::{self};


// User inserts an integer value
// from the standart input (keyboard)
// and return this value.
pub fn read_integer() -> u32 {

    let mut key = 0;
    let mut number_as_text = String::new();

    println!("\nEnter the Encryption key: ");

        io::stdin()
            .read_line(&mut number_as_text)
            .expect("failed to read from stdin");

        let trimmed = number_as_text.trim();
        match trimmed.parse::<u32>() {
            Ok(i) => key = i,
            Err(..) => println!("\nThis was not an integer: {}", trimmed)
        };

    key

}


// User insterts a message (one line only is permitted)
// using the the standart input (keyboard)
// and return this message.
pub fn read_filename() -> String {

    let filename = rpassword::prompt_response_stdout("\nEnter filename: ").unwrap();

    filename

}
