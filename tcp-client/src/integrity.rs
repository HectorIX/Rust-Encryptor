/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/




extern crate crypto;


use self::crypto::digest::Digest;
use self::crypto::whirlpool::Whirlpool;
use self::crypto::sha3::Sha3;



pub fn sha3_512(context:String) -> String {


    let mut hasher = Sha3::sha3_512();


    hasher.input_str(&context);
    let sha_512 = hasher.result_str();

    sha_512
}


pub fn whirlpool(context:String) -> String {

    let mut whirlpool_hasher = Whirlpool::new();


    whirlpool_hasher.input_str(&context);
    let whirlpool = whirlpool_hasher.result_str();

    whirlpool
}
